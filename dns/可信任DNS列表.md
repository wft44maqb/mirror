> DNS over HTTPS（縮寫：DoH）是一個進行安全化的域名解析方案。其意義在於以加密的HTTPS協定進行DNS解析請求，避免原始DNS協定中使用者的DNS解析請求被竊聽或者修改的問題（例如中間人攻擊）來達到保護使用者隱私的目的。Google及Mozilla基金會正在測試此協定，提高網路安全性。

| 提供商 | dns網址 | 功能·特點 | 官網 | 備註 |
|--|--|--|--|--|
| 歐美 NextDNS Mozilla隱私加強版 | https://firefox.dns.nextdns.io/ | 加入了Mozilla TRR計劃，中國大陸附近有多個伺服器 | https://nextdns.io/zh |  |
| 美國 Cloudflare 1.1.1.1 Mozilla隱私加強版 | https://mozilla.cloudflare-dns.com/dns-query | 加入了Mozilla TRR計劃，中國大陸境內有多個伺服器 | https://1.1.1.1/ | 跟中國百度、京東關係密切 |
| 歐美 NextDNS | (請自行註冊NextDNS帳戶 以獲取專屬dns網址) | 可阻擋部分廣告&網絡追踪器&惡意網域 | https://nextdns.io/zh | 中國大陸附近有多個伺服器，香港台灣日本等等 |
| 台灣 Quad101 | https://101.101.101.101/dns-query |  | https://101.101.101.101/index.html |  |
| 美國 Google公共DNS | https://8.8.8.8/dns-query 或 https://8.8.8.8/resolve? |  | https://8.8.8.8/ |  |
| 美國 Cloudflare 1.1.1.1 | https://1.1.1.1/dns-query |  |  |  |
| 待更新 |  |  |  |  |
|  |  |  |  |  |

| 提供商 | 香港伺服器 | 台灣伺服器 | 中國大陸伺服器 | 日本伺服器 | 韓國伺服器 | 新加坡伺服器 | 澳洲伺服器 | 備註 |
|--|--|--|--|--|--|--|--|--|
| 歐美 NextDNS | ✔ | ✔ | ✘ | ✔ |  |  |  |  |
| 美國 Cloudflare | ✔ | ✔ | ✔ |  |  |  |  |  |
| 台灣 Quad101 | ✘ | ✔ | ✘ | ✘ | ✘ | ✘ | ✘ |  |
| 美國 Google | ✔ | ✔ | ✘ | ✔ | ✔ | ✔ | ✔ |  |
|  |  |  |  |  |  |  |  |  |



---
###### Trusted Recursive Resolver（TRR）政策
https://wiki.mozilla.org/Trusted_Recursive_Resolver

###### DNS over HTTPS - 維基百科，自由的百科全書
https://zh.wikipedia.org/zh-cn/DNS_over_HTTPS#%E5%85%AC%E5%85%B1DNS

###### Firefox 中的 DNS-over-HTTPS（DoH）
https://support.mozilla.org/zh-TW/kb/firefox-dns-over-httpsdoh


---
###### Trusted Recursive Resolver（TRR）政策
https://wiki.mozilla.org/Trusted_Recursive_Resolver

###### DNS over HTTPS - 維基百科，自由的百科全書
https://zh.wikipedia.org/zh-cn/DNS_over_HTTPS#%E5%85%AC%E5%85%B1DNS

###### Firefox 中的 DNS-over-HTTPS（DoH）
https://support.mozilla.org/zh-TW/kb/firefox-dns-over-httpsdoh
