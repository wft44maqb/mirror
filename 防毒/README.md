# 權威殺毒軟體評測機構
### [AV-Comparatives](https://www.av-comparatives.org/consumer/)
AV-Comparatives是一個被奧地利政府承認的非營利組織，也是一個國際性的獨立測試機構，因提供針對電腦安全軟體的綜合性與客觀性評測結果而聞名。AV-Comparatives每年皆會發布固定的年度評測報告，評測報告中會以客觀角度指出各家廠牌防毒軟體的優缺點及建議。 [維基百科](https://zh.wikipedia.org/zh-cn/AV-Comparatives)
### [Virus Bulletin](https://www.virusbulletin.com/testing/vb100/)
Virus Bulletin是一個關於流氓軟體與垃圾郵件防護、偵測以及移除的雜誌。它經常給出一些關於最新病毒威脅的分析，發表探索反病毒領域的最新進展，採訪反病毒的專家，並且評估現存的反病毒產品。 [維基百科](https://zh.wikipedia.org/zh-cn/Virus_Bulletin)
### [AV-TEST](https://www.av-test.org/en/antivirus/home-users/)
AV-Test反病毒測試有限責任公司2004年在馬格德堡創立。在這之前，AV-Test只是馬格德堡大學的一個科研專案。AV-Test在反病毒研究和資料安全領域已經有15年的歷史經驗。 [維基百科](https://zh.wikipedia.org/zh-cn/AV-TEST)

話說最近一年通過了以上任一平台嚴謹測試的中國防毒軟件有以下這些:
- [安天 (Antiy或AVL)](https://www.av-test.org/en/antivirus/mobile-devices/manufacturer/antiy/)
###### 安天就是金山獵豹用來洗白的 就像卡巴斯基的Northguard

- [騰訊](https://www.virusbulletin.com/vb100/testing/tencent)
###### 騰訊國際版官網沒有备案👍
###### 騰訊電腦管家國際版 透過這個項目下載: [https://gitlab.com/wft44maqb/mirror/](https://gitlab.com/wft44maqb/mirror/-/tree/main/%E9%98%B2%E6%AF%92/%E4%B8%AD%E5%9C%8B%E8%A3%BD/Tencent%20PC%20Manager)
###### 騰訊電腦管家國際版 透過官方網站下載: https://www.pcmgr-global.com/

- [瑞星](https://www.virusbulletin.com/vb100/testing/rising)
###### 瑞星國際版官網有备案👎

- [奇安信](https://www.virusbulletin.com/vb100/testing/qianxin-group)
##### 「360牵手国资，奇安信拥抱腾讯」—[jiemian.com](https://www.jiemian.com/article/5507725.html)
##### 奇安信好像沒有民用版本 只有商用版本

![](https://gitlab.com/wft44maqb/mirror/-/raw/main/%E9%98%B2%E6%AF%92/image.png)

---

### 卡巴斯基免費key
###### 卡巴斯基測試版key
- D8BKN-PAYDW-ASSTN-GV1HD
- YQWEJ-YW281-YMGSY-U8HDW
- 4BFHZ-EB4HB-VQKWR-BUBEY
- YWRXU-1C4N7-6ACG2-82GU8
- XEFJY-2SFAU-PBBWJ-GAGX7

key來源: [Beta-Testing Guide (English) | Beta Testing (kaspersky.com)](https://eap.kaspersky.com/topic/62/beta-testing-guide-english/3)
