### 如何測試網站是否被中國防火長城封鎖
這裡有一些工具可以檢查網站是否在中國被封鎖。
-   [Greatfire.org](https://blocky.greatfire.org/)
-   [Greatfire.org舊版](https://zh.greatfire.org/analyzer)
-   [China Firewall Test](http://www.chinafirewalltest.com/)
-   [Comparitech](https://www.comparitech.com/privacy-security-tools/blockedinchina/)
-   [OONI Probe](https://ooni.org/) 下載: [https://gitlab.com/wft44maqb/mirror/](https://gitlab.com/wft44maqb/mirror/-/tree/main/%E7%B6%B2%E8%B7%AF%E5%AF%A9%E6%9F%A5%E5%81%B5%E6%B8%AC)
