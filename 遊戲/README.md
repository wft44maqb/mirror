- faceit_1 镜像下载点: https://gitlab.com/wft44maqb/mirror/-/raw/main/%E9%81%8A%E6%88%B2/faceit/FACEIT-setup-latest.exe?inline=false
- faceit_2 镜像下载点: https://gitlab.com/wft44maqb/mirror/-/raw/main/%E9%81%8A%E6%88%B2/faceit/FACEITInstaller_64_%E9%98%B2%E5%A4%96%E6%8E%9B.exe?inline=false

- 5e国际服 镜像下载点: https://gitlab.com/wft44maqb/mirror/-/raw/main/%E9%81%8A%E6%88%B2/5E/5E%20Arena/5EArena-1.1.7.exe?inline=false

---

- faceit_1 官方下载点: https://faceit-client.faceit-cdn.net/release/FACEIT-setup-latest.exe
- faceit_2 官方下载点: https://anticheat-client.faceit-cdn.net/FACEITInstaller_64.exe

- 5e国际服 官方下载点: https://oss.5earenacdn.com/client/releases/5EArena-1.1.7.exe
