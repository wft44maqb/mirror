# 六四30週年：天安門鮮血帶給共產世界的勇氣 - The News Lens 關鍵評論網 https://activity.thenewslens.com/tiananmen-2019/

### 相關報導: (google搜尋)
台灣公視: https://www.google.com/search?q=site%3Apts.org.tw+OR+site%3Acts.com.tw+%E5%85%AD%E5%9B%9B%E4%BA%8B%E4%BB%B6+OR+%E5%85%AD%E5%9B%9B%E5%A4%A9%E5%AE%89%E9%96%80%E4%BA%8B%E4%BB%B6 \
香港公廣: https://www.google.com/search?q=site%3Arthk.hk+%E5%85%AD%E5%9B%9B%E4%BA%8B%E4%BB%B6+OR+%E5%85%AD%E5%9B%9B%E5%A4%A9%E5%AE%89%E9%96%80%E4%BA%8B%E4%BB%B6 \
英國公視: https://www.google.com/search?q=site%3Abbc.com+OR+site%3Abbc.co.uk+%E5%85%AD%E5%9B%9B%E4%BA%8B%E4%BB%B6+OR+%E5%85%AD%E5%9B%9B%E5%A4%A9%E5%AE%89%E9%96%80%E4%BA%8B%E4%BB%B6 \
澳洲公廣: https://www.google.com/search?q=site%3Asbs.com.au+OR+site%3Aabc.net.au+OR+site%3Aabcaustralia.net.au+%E5%85%AD%E5%9B%9B%E4%BA%8B%E4%BB%B6+OR+%E5%85%AD%E5%9B%9B%E5%A4%A9%E5%AE%89%E9%96%80%E4%BA%8B%E4%BB%B6 \
德國公廣: https://www.google.com/search?q=site%3Adw.com+%E5%85%AD%E5%9B%9B%E4%BA%8B%E4%BB%B6+OR+%E5%85%AD%E5%9B%9B%E5%A4%A9%E5%AE%89%E9%96%80%E4%BA%8B%E4%BB%B6 \
法國公廣: https://www.google.com/search?q=site%3Arfi.fr+%E5%85%AD%E5%9B%9B%E4%BA%8B%E4%BB%B6+OR+%E5%85%AD%E5%9B%9B%E5%A4%A9%E5%AE%89%E9%96%80%E4%BA%8B%E4%BB%B6 \

 \
![](https://gitlab.com/wft44maqb/mirror/-/raw/main/%E9%87%8D%E5%A4%A7%E4%BA%8B%E4%BB%B6/2000%E5%B9%B4%E5%89%8D/64/__%E5%BF%97%E7%A5%BA%E4%B8%83%E4%B8%83__%E5%85%AD%E5%9B%9B%E5%A4%A9%E5%AE%89%E9%96%80%E4%BA%8B%E4%BB%B6_30_%E9%80%B1%E5%B9%B4_%E7%95%B6%E5%B9%B4%E4%B8%AD%E5%9C%8B%E7%AB%9F%E7%84%B6%E5%B7%AE%E9%BB%9E%E5%B0%B1%E8%AE%8A%E6%88%90%E4%B8%80%E5%80%8B%E6%B0%91%E4%B8%BB%E5%9C%8B%E5%AE%B6%E4%BA%86___%E5%B7%A6%E9%82%8A%E9%84%B0%E5%B1%85%E8%A7%80%E5%AF%9F%E6%97%A5%E8%A8%98__EP010.mp4)

![](https://gitlab.com/wft44maqb/mirror/-/raw/main/%E9%87%8D%E5%A4%A7%E4%BA%8B%E4%BB%B6/2000%E5%B9%B4%E5%89%8D/64/%E4%B8%8D%E5%8F%AA%E6%98%AF%E4%B8%AD%E5%9C%8B_%E5%BE%9E%E5%85%B1%E7%94%A2%E4%B8%96%E7%95%8C%E8%84%88%E7%B5%A1%E7%9C%8B%E5%85%AD%E5%9B%9B30%E9%80%B1%E5%B9%B4_%E5%9C%8B%E9%9A%9B%E5%A4%A7%E9%A2%A8%E5%90%B9.mp4)

![](https://gitlab.com/wft44maqb/mirror/-/raw/main/%E9%87%8D%E5%A4%A7%E4%BA%8B%E4%BB%B6/2000%E5%B9%B4%E5%89%8D/64/%E5%85%AD%E5%9B%9B%E4%BA%8B%E4%BB%B6BBC%E5%A4%A9%E5%AE%89%E9%96%80%E7%8F%BE%E5%A0%B4%E5%A0%B1%E9%81%93%E5%85%A8%E7%B4%80%E9%8C%84.mp4)

![](https://gitlab.com/wft44maqb/mirror/-/raw/main/%E9%87%8D%E5%A4%A7%E4%BA%8B%E4%BB%B6/2000%E5%B9%B4%E5%89%8D/64/%E5%85%AD%E5%9B%9B%E4%BA%8B%E4%BB%B6_%E5%A5%89%E5%91%BD%E5%89%8D%E5%BE%80%E5%8C%97%E4%BA%AC%E5%9F%B7%E8%A1%8C%E6%88%92%E5%9A%B4%E4%BB%BB%E5%8B%99%E7%9A%84%E5%89%8D%E4%B8%AD%E5%9C%8B%E8%A7%A3%E6%94%BE%E8%BB%8D__BBC_News_%E4%B8%AD%E6%96%87__%E6%9D%8E%E6%9B%89%E6%98%8E_%E5%85%AB%E4%B9%9D%E6%B0%91%E9%81%8B_.mp4)

![](https://gitlab.com/wft44maqb/mirror/-/raw/main/%E9%87%8D%E5%A4%A7%E4%BA%8B%E4%BB%B6/2000%E5%B9%B4%E5%89%8D/64/%E5%85%AD%E5%9B%9B%E4%BA%8B%E4%BB%B6_%E5%BB%A3%E5%A0%B4%E5%82%99%E5%BF%98%E9%8C%84__BBC_News_%E4%B8%AD%E6%96%87__%E5%A4%A9%E5%AE%89%E9%96%80_1989.mp4)

![](https://gitlab.com/wft44maqb/mirror/-/raw/main/%E9%87%8D%E5%A4%A7%E4%BA%8B%E4%BB%B6/2000%E5%B9%B4%E5%89%8D/64/screencapture-zh-wikipedia-org-zh-cn-2021-07-08-08_42_50.jpg)
![](https://gitlab.com/wft44maqb/mirror/-/raw/main/%E9%87%8D%E5%A4%A7%E4%BA%8B%E4%BB%B6/2000%E5%B9%B4%E5%89%8D/64/screencapture-zh-wikipedia-org-zh-cn-2021-07-08-08_42_50-2.jpg)
