### 如何測試網站是否被中國防火長城封鎖
這裡有一些工具可以檢查網站是否在中國被封鎖。
-   [Greatfire.org](https://blocky.greatfire.org/)
-   [Greatfire.org舊版](https://zh.greatfire.org/analyzer)
-   [China Firewall Test](http://www.chinafirewalltest.com/)
-   [Comparitech](https://www.comparitech.com/privacy-security-tools/blockedinchina/)
